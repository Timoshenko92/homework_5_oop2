package com.Zadacha2;

/**
 * Created by Admin on 09.10.2016.
 */
public abstract class Animal {
    String name, color;
    int countOfLegs, countOfWings;
    public void animal(String name,int countOfLegs, int countOfWings, String color ){
        this.name=name;
        this.countOfLegs=countOfLegs;
        this.countOfWings=countOfWings;
        this.color=color;
    }
    public enum type {
        MAMMAL,
        AMPHIBIAN,
        BIRD,
        FISH
    }
    public abstract void voice (String name, String color);
}
